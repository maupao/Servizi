# servizi.linux.it

All'indirizzo http://servizi.linux.it/ è esposta una istanza pubblica di [SandStorm](http://sandstorm.io/), destinata ad erogare servizi di pubblica utilità.

Questo repository è inteso a raccogliere segnalazioni e richieste in merito alla piattaforma, da eventulmente riportare in upstream al progetto SandStorm e/o su cui coinvolgere volontari.
